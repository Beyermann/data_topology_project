Number of points per sample m=800
Number of samples n=100
Object=torus
Inner Radius R=0.5
Outer Radius r=0.25
ExpansionFactor e=3.2
Factor for cutting the persistence of unhandeled points c=2
