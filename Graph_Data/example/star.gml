graph [
  name "star_graph(6)"
  node [
    id 0
    label 0
    bipartite 0
  ]
  node [
    id 1
    label 1
    bipartite 1
  ]
  node [
    id 2
    label 2
    bipartite 1
  ]
  node [
    id 3
    label 3
    bipartite 1
  ]
  node [
    id 4
    label 4
    bipartite 1
  ]
  node [
    id 5
    label 5
    bipartite 1
  ]
  node [
    id 6
    label 6
    bipartite 1
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
]
