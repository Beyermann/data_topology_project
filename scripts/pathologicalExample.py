#!/usr/bin/env python3

import networkx as nx
import matplotlib.pyplot as plt
import sys

def main(path):
  G = nx.Graph()
  G.add_nodes_from([1,2,3,4,5,6,7,8,9])
  G.add_edges_from([(1,2),(2,3),(3,4),(3,5),(4,6),(4,7),(5,8),(5,9),(6,7),(6,8),(6,9),(7,8),(7,9),(8,9)])
  nx.write_gml(G, path)
  plt.figure()
  nx.draw(G)
  plt.show()

if __name__ == "__main__":
  main(sys.argv[1])
