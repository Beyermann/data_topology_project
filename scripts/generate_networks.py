#!/usr/bin/env python3

import networkx as nx

def main():
  gnm_path = "/home/jens/Uni/data_topology/Project/Graph_Data/Synthetic_Gnm_2/gnm_"
  ba_path = "/home/jens/Uni/data_topology/Project/Graph_Data/Synthetic_Barabasi-Albert_2/bag_"
  er_path = "/home/jens/Uni/data_topology/Project/Graph_Data/Synthetic_Erdos-Reny_2/erg_"
  er_red_path = "/home/jens/Uni/data_topology/Project/Graph_Data/Synthetic_Erdos_Reny_reduced_2/err_"
  #ws_path = "/home/jens/Uni/data_topology/Project/Graph_Data/Synthetic_Watts-Strogatz/"
  #cws_path = "/home/jens/Uni/data_topology/Project/Graph_Data/Synthetic_Watts-Strogatz_connected/"
  #nws_path = "/home/jens/Uni/data_topology/Project/Graph_Data/Synthetic_Neumann-Watts-Strogatz/"
  plt_path = "/home/jens/Uni/data_topology/Project/Graph_Data/Synthetic_Power_Law_Trees/plt_"
  hh_path = "/home/jens/Uni/data_topology/Project/Graph_Data/Synthetic_Havel_Hakimi/hhg_"
  dst_path = "/home/jens/Uni/data_topology/Project/Graph_Data/Synthetic_Degree_Sequence_Trees/dst_"
  rds_path = "/home/jens/Uni/data_topology/Project/Graph_Data/Synthetic_Random_Degree_Sequence_Graphs/rds_"
  sgnp_path = "/home/jens/Uni/data_topology/Project/Graph_Data/Synthetic_Small_Gnp/sgnp_"
  sba_path = "/home/jens/Uni/data_topology/Project/Graph_Data/Synthetic_example_rnd/example_"
  #tree_sequence = nx.random_powerlaw_tree_sequence(50, gamma=3,tries=500)

  for k in range(100):
    
    
    n = 1000
    m = int((1000 * 1000 / 2) * 0.07)
    p = 0.07
    p_red = 0.05
    n_small_gnm = 30
    m_small = int((100 * 99/2) * 0.007)
    
    
    
    #plt_networkt = nx.random_powerlaw_tree
    
    #dst_network = nx.degree_sequence_tree(tree_sequence)
    #hh_network = nx.havel_hakimi_graph(tree_sequence)
    #rds_network = nx.random_degree_sequence_graph(tree_sequence)
    
    sba_network = nx.erdos_renyi_graph(n_small_gnm,p)
    #sba_network = nx.barabasi_albert_graph(n_small_gnm,(m_small//n_small_gnm))
    
    #gnm_network = nx.gnm_random_graph(n,m)
    #ba_network = nx.barabasi_albert_graph(n, (m//n) )
    #er_network = nx.erdos_renyi_graph(n,p)
    #er_red_network = nx.erdos_renyi_graph(n,p_red)
    #ws_network = nx.watts_strogatz_graph(n, (m//n), 0.3)
    #cws_network = nx.connected_watts_strogatz_graph(n, (m//n), 0.3)
    #nws_network = nx.newman_watts_strogatz_graph(n, (m//n), 0.3)
    
    #nx.write_gml(plt_networkt, plt_path + str(k) + ".gml")
    #nx.write_gml(dst_network, dst_path + str(k) + ".gml")
    #nx.write_gml(hh_network, hh_path + str(k) + ".gml")
    #nx.write_gml(rds_network, rds_path + str(k) + ".gml")
    #nx.write_gml(sgnp_network, sgnp_path + str(k) + ".gml")
    nx.write_gml(sba_network, sba_path + str(k) + ".gml")
    #nx.write_gml(gnm_network, gnm_path  + str(k) + ".gml")
    #nx.write_gml(ba_network, ba_path  + str(k) + ".gml")
    #nx.write_gml(er_network, er_path + str(k) + ".gml")
    #nx.write_gml(er_red_network, er_red_path + str(k) + ".gml")
    #nx.write_gml(ws_network, ws_path + str(k) + ".gml")
    #nx.write_gml(cws_network, cws_path + str(k) + ".gml")
    #nx.write_gml(nws_network, nws_path + str(k) + ".gml")
    
if __name__ == "__main__":
  main()
