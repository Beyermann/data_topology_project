#!/usr/bin/env python
import numpy as np
from scipy.special import binom

def C(n,p):
  if n == 1:
    return 1
  else:
    print(n)
    sum = np.sum([C(i,p) * binom(n-1,i-1)*(1-p)**(i*(n-i)) for i in range(1,n) ])
    return 1-sum
  
if __name__ == "__main__":
  n = 1000
  p = 0.07
  print("probability of being connected: " + str(C(n,p)) + "\n")
