#!/usr/bin/env python3

import os

def main():
  #input_path = "/home/jens/Uni/data_topology/Project/Graph_Data/Synthetic_Gnm/"
  input_path = "/home/jens/Uni/data_topology/Project/reddit_results/graphs/"
  output_path = "/home/jens/Uni/data_topology/Project/syntheticNetworkResults"
  graphs = [g for g in sorted(os.listdir(input_path))]
  for g in graphs:
    #popen = subprocess.Popen(args, stdout=subprocess.PIPE)
    #popen.wait()
    #output = popen.stdout.read()
    #print(output)
    cmd = "/home/jens/Uni/data_topology/Aleph/build/examples/network_analysis --output " + output_path + " " + input_path + g
    os.system(cmd)

if __name__ == "__main__":
  main()
