#!/usr/bin/env python3

import os
import sys

def main(argv):
  input_path = argv[1]
  output_path = argv[2]
  #input_path = "/home/jens/Uni/data_topology/Project/Graph_Data/Synthetic_Gnm/"
  #input_path = "/home/jens/Uni/data_topology/Project/reddit_results/graphs/"
  #input_path = "/home/jens/Uni/data_topology/Project/Graph_Data/Synthetic_Gnm_2/"
  #input_path = "/home/jens/Uni/data_topology/Project/Graph_Data/Synthetic_Random_Degree_Sequence_Graphs/"
  #input_path = "/home/jens/Uni/data_topology/Project/Graph_Data/Synthetic_Small_Gnp/"
  #input_path = "/home/jens/Uni/data_topology/Project/Graph_Data/example/"
  #output_path = "/home/jens/Uni/data_topology/Project/syntheticNetworkResults"
  #output_path = "/home/jens/Uni/data_topology/Project/syntheticNetworkResults/example_diagrams"
  graphs = [g for g in sorted(os.listdir(input_path))]
  for g in graphs:
    #popen = subprocess.Popen(args, stdout=subprocess.PIPE)
    #popen.wait()
    #output = popen.stdout.read()
    #print(output)
    cmd = "/home/jens/Uni/data_topology/Aleph/build/examples/network_analysis_landscape --output " + output_path + " " + input_path + g
    os.system(cmd)

if __name__ == "__main__":
  main(sys.argv)
