#!/usr/bin/env python
import numpy as np
import networkx as nx
import matplotlib.pyplot as plt
import sys


def main(argv):
  path = str(argv[1])

  #simple_graph = nx.gnm_random_graph(7,10)
  #path_short = nx.path_graph(3)
  #path_long = nx.path_graph(5)
  #cycle_small = nx.cycle_graph(4)
  #cycle_big = nx.cycle_graph(6)
  #star = nx.star_graph(6)
  #wheel = nx.wheel_graph(6)
  #balanced_tree = nx.balanced_tree(2,3)
  #complete_graph = nx.complete_graph(5)
  triangle = nx.cycle_graph(3)
  
  #nx.write_gml(simple_graph, path + "/simple_graph.gml")
  #nx.write_gml(path_short, path + "/path_short.gml")
  #nx.write_gml(path_long, path + "/path_long.gml")
  #nx.write_gml(cycle_small, path + "/cycle_small.gml")
  #nx.write_gml(cycle_big, path + "/cycle_big.gml")
  #nx.write_gml(star, path + "/star.gml")
  #nx.write_gml(wheel, path + "/wheel.gml")
  #nx.write_gml(balanced_tree, path + "/balanced_tree.gml")
  #nx.write_gml(complete_graph, path + "/complete_graph.gml")
  nx.write_gml(triangle,path + "/triangle.gml")
  #plt.figure()
  #nx.draw(balanced_tree)
  #plt.show()
if __name__ == "__main__":
  main(sys.argv)
