#!/usr/bin/python
# -*- coding: utf-8 -*-
# (C) IWR 2011 <your name>

"""NAME
        %(prog)s - <one line short description>
    
SYNOPSIS
        %(prog)s [--help]
        
DESCRIPTION
        none
        
FILES  
        none
     
SEE ALSO
        nothing
        
DIAGNOSTICS
        none
        
BUGS    
        none
    
AUTHOR
        <your name>, <email>@iwr.uni-heidelberg.de
        {version}
"""

version="$Id$"
#--------- Classes, Functions, etc ---------------------------------------------
class Example(object):
    """This shows how the docu should be included in a class...
    """
    x=0

def examplefunc():
    """This shows how the docu should be included in a function...
    """
    pass
#-------------------------------------------------------------------------------
import argparse
class MyHelpFormatter(argparse.RawTextHelpFormatter):
    # It is necessary to use the new string formatting syntax, otherwise
    # the %(prog) expansion in the parent function is not going to work
    def _format_text(self, text):
        if '{version}' in text:
            text = text.format(version=version)
        return super(MyHelpFormatter, self)._format_text(text)

def ManOptionParser():
    return argparse.ArgumentParser(description=__doc__, 
                                       formatter_class=MyHelpFormatter)

#-------------------------------------------------------------------------------
#    Main 
#-------------------------------------------------------------------------------
if __name__=="__main__":
    import sys

    # Command line option parsing example
    parser = ManOptionParser()
    # Note that -h (--help) is added by default and uses the help strings of
    # the other options. The variables containing the options are automatically
    # created.
    
    # Boolean option (default is !action)
    parser.add_argument('-q', '--quiet', action='store_true', dest='quiet',
                        help="don't print status messages to stdout")
    # Option
    parser.add_argument('-l', dest='logfile',action='store',
                        metavar='LOGFILE',help='Log file')

    # Option (default: default=None, action=store)
    parser.add_argument('-v', dest='verbose_level', type=int,
                        metavar='VERBOSITY', help='Verbosity level (0-99)')

    # Mandatory arguments (see nargs)
    parser.add_argument('files', nargs=2, help='input and output file names')

    args = parser.parse_args()
    if not args.quiet: print("Replace template: add your program here")
