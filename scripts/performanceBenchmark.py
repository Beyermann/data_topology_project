#!/usr/bin/env python
import sys
import os
import numpy as np

def genDiagrams(nCritPoints, nDiagrams, distribution):
  
  for index in range(nDiagrams):
    filename = "/tmp/" + str(index) + "d0.txt"
    f = open(filename,"w")
    f.write("# Random sampled persisistence diagram \n#\n# Dimension\t: 0\n# Entries\t: " + str(nCritPoints) + "\n# Betti number: ndef\n")
    
    for i in range(nCritPoints):
      point = distribution(0,1,2)
      f.write(str(point[0]) + " " + str(point[1]) + "\n")
    
    f.close()


def main (minimumDatasize, maximumDatasize, stepsize):
  
  # generate data for different data complexity
  for m in np.arange(minimumDatasize, maximumDatasize+stepsize,stepsize):
    # create data with sample density m
    genDiagrams(m,2,np.random.uniform)
    # maybe store data elsewhere temporarily
    
    # calculate wasserstein distances
    cmd = "/home/jens/Uni/data_topology/Aleph/build/src/tools/topological_distance --wasserstein /tmp/*d0.txt"
    os.system(cmd)
    # store performance and distance data in sensible folder
    cmd = "mv performance.txt /home/jens/Uni/data_topology/Project/benchmark/wasserstein/performance_" + str(m) + ".txt"
    os.system(cmd)
    # calculate landscape distances
    cmd = "/home/jens/Uni/data_topology/Aleph/build/examples/distance-matrix_from_diagrams /tmp/*d0.txt"
    os.system(cmd)
    # store performance and distance data in sensible folder
    cmd = "mv performance.txt /home/jens/Uni/data_topology/Project/benchmark/landscape/performance_" + str(m) + ".txt"
    os.system(cmd)
    cmd = "rm distances.txt"
    os.system(cmd)
    # remove data from /tmp/ or from wherever its stored
    cmd = "rm /tmp/*d0.txt"
    os.system(cmd)
    
  
if __name__ == "__main__":
  minimumDatasize = int(sys.argv[1])
  maximumDatasize = int(sys.argv[2])
  stepsize = int(sys.argv[3])
  main(minimumDatasize,maximumDatasize, stepsize)
