#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt

x=np.arange(100,1100,100)
l_1 = np.array([0.00002,0.0001,0.0003,0.0005,0.01,0.01,0.03,0.05,0.05,0.07])
w_1 = np.array([0.03,0.32,1.55,3.86,11.01,23.71,35.32,100.1,178.27,155.29 ])

l_2 = np.array([0.00003,0.0001,0.0003,0.0007,0.01,0.02, 0.03,0.05,0.06,0.07])
w_2 = np.array([0.03,0.51,1.89,6.87,19.68,17.91,48.27,111.96,152.54,220.89])

l_3 = np.array([0.00004,0.0001,0.0004,0.001,0.01,0.02,0.03,0.05,0.06,0.09])
w_3 = np.array([0.03,0.58,2.45,3.50,15.14,31.56,39.35,53.70,132.68,145.11])

l_4 = np.array([])
w_4 = np.array([])

l = l_1 + l_2 + l_3
w = w_1 + w_2 + w_3

l /= 3.
w /= 3.

#plt.semilogy(x,y_1)
#plt.semilogy(x,y_2)
plt.plot(x,l)
plt.plot(x,w)
plt.xlabel('Anzahl der Punkte im Persistenzdiagramm')
plt.ylabel('CPU Minuten')
plt.show()
