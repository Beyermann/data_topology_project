#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import sys

def plot_diagram(filename, targetFile=None):
    with open(filename) as f:
        dataFile = f.readlines()
    data = np.array([line.strip('\n').split('\t') for line in dataFile if line[0] != "#"])
    data = data.astype(np.float)
    
    plt.plot(data[:,0],data[:,1], 'o',c='b',label='finite point')
    
    ymin,ymax = plt.gca().get_ylim()
    xmin,xmax = plt.gca().get_xlim()
    
    # handle the case that only unpaired points are in the diagram
    if (ymin,ymax == 0):
      xmin,xmax = np.min(data[:,0]),np.max(data[:,0])
      ymin,ymax = xmin,xmax
    
    plt.gca().set_xlim(0,ymax + ymin)
    plt.gca().set_ylim(0,ymax + ymin)
    boolean = data[:,1] > ymax
    unpaired = data[boolean,:]
    unpaired[:,1] = np.array([(ymax + ymin) * 0.98]*unpaired.shape[0])
    plt.plot(unpaired[:,0],unpaired[:,1],'o',c='r',label='unpaired point')
    
    lims = [np.min([plt.gca().get_xlim(), plt.gca().get_ylim()]), np.max([plt.gca().get_xlim(), plt.gca().get_ylim()]), ]
    plt.plot(lims, lims, 'k-', alpha=0.75, zorder=0)
    
    #red_patch = mpatches.Patch(color='r', label='unpaired point')
    #blue_batch = mpatches.Patch(color='b', label='finite point')
    plt.grid()
    plt.axes().set_aspect('equal')
    plt.legend()
    if (targetFile):
        plt.savefig(targetFile)
    plt.show()
    #print(data)
    
if __name__ == "__main__":
  filename = sys.argv[1]
  if len(sys.argv) >= 3:
    targetFile = sys.argv[2]
  else:
    targetFile = None
  
  plot_diagram(filename, targetFile)
