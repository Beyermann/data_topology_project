#! /usr/bin/env python3
import numpy as np
import matplotlib.pyplot as plt
import sys
import os

def createExamplePlots_experimental ():
  function_1 = [lambda x: x-1, lambda x: -x+6, lambda x: 0]
  function_2 = np.array([lambda x: x-2, lambda x: -x+3, lambda x: 0])
  function_3 = np.array([lambda x: x-3, lambda x: -x+7, lambda x: 0])
  function_4 = np.array([lambda x: x-4, lambda x: -x+5, lambda x: 0])
  
  x = np.linspace(0,7,1000)
  
  domain_1 = [(x >= 1) & (x < 3.5), (x >= 3.5) & (x < 6), (x >=6) | (x < 1)]
  domain_2 = [(x >= 2) & (x < 2.5), (x >= 2.5) & (x < 3), (x >=3) | (x < 2)] 
  domain_3 = [(x >= 3) & (x < 5), (x >= 5) & (x < 7), (x >=7) | (x < 3)]
  domain_4 = [(x >= 4) & (x < 4.5), (x >= 4.5) & (x < 5), (x >=5) | (x < 4)]
  
  y_1 = np.piecewise(x,domain_1, function_1)
  y_2 = np.piecewise(x,domain_2, function_2)
  y_3 = np.piecewise(x,domain_3, function_3)
  y_4 = np.piecewise(x,domain_4, function_4)
  y_white = [0 for el in x]
  plt.figure()
  plt.plot(x,y_1)
  plt.plot(x,y_2)
  plt.plot(x,y_3)
  plt.plot(x,y_4)
  plt.plot(x,y_white,c="white",linewidth=2.5)
  plt.plot()
  plt.show()
  #landscape_layer = np.piecewise(

def createExamplePlots (intervals, x, path,color=None):
  
  function = [None] *len(intervals)
  domain = [None] * len(intervals)
  y = [None] * len(intervals)
  y_white = [0 for el in x]
  plt.figure()

  for i in range(len(intervals)):
    function[i] = np.array([lambda x: x-intervals[i][0], lambda x: -x+intervals[i][1], lambda x: 0])
    domain[i] = generateDomain(intervals[i][0],intervals[i][1],x)
    y[i] = np.piecewise(x,domain[i],function[i])
    if color:
      plt.plot(x,y[i],c=color)
    else:
      plt.plot(x,y[i])
  
  plt.plot(x,y_white,c="white",linewidth=2.5)
  #colorEnvelope()
  #plt.savefig(path)
  #plt.show()
  
def colorEnvelope():
  function = np.array([lambda x: x-1, lambda x: -x + 6, lambda x: x-2, lambda x: -x+7])
  domain = [np.linspace(1,3.5,400),np.linspace(3.5,4,100),np.linspace(4,4.5,100),np.linspace(4.5,7,400)]
  for i in range ( len(function) ):
    plt.plot(domain[i],function[i](domain[i]),c="red")
  #plt.show()
  
def colorEnvelope_1():
  function = np.array([lambda x: x-1, lambda x: -x + 5, lambda x: x-2, lambda x: -x+6])
  domain = [np.linspace(1,3,400),np.linspace(3,3.5,100),np.linspace(3.5,4,100),np.linspace(4,6,400)]
  for i in range ( len(function) ):
    plt.plot(domain[i],function[i](domain[i]),c="red")
    
def colorEnvelope_2():
  function = np.array([ lambda x: x-2, lambda x: -x+5])
  domain = [np.linspace(2,3.5,200),np.linspace(3.5,5,200)]
  for i in range ( len(function) ):
    plt.plot(domain[i],function[i](domain[i]),c="red")
    
def colorEnvelope_3():
  function = np.array([ lambda x: x-2, lambda x: -x+3])
  domain = [np.linspace(2,2.5,200),np.linspace(2.5,3,200)]
  for i in range ( len(function) ):
    plt.plot(domain[i],function[i](domain[i]),c="red")
  
def generateDomain (border1, border2, x):
  domain = [(x >= border1) & (x < (border1+border2)/2 ), (x >= (border1+border2)/2) & (x < border2), (x >=border2) | (x < border1)]
  return domain

if __name__ == "__main__":
  path = sys.argv[1]
  if (os.path.exists(path)):
    raise RuntimeError("File already exists")

  intervals = [[1,6],[1,5],[2,3],[2,7]]
  x = np.linspace(0,7,10000)

  createExamplePlots(intervals,x,path,"blue")
  plt.ylim(0,3)
  plt.xlim(0,8)
  plt.savefig(path+".svg")
  plt.show()
  createExamplePlots(intervals,x,path,"blue")
  intervals = [[1,5],[2,6],[2,3]]
  colorEnvelope()
  plt.ylim(0,3)
  plt.xlim(0,8)
  plt.savefig(path+"_0.svg")
  plt.show()
  createExamplePlots(intervals,x,path+"_1","blue")
  plt.ylim(0,3)
  plt.xlim(0,8)
  plt.savefig(path+"_1.svg")
  plt.show()
  createExamplePlots(intervals,x,path+"_1","blue")
  colorEnvelope_1()
  plt.ylim(0,3)
  plt.xlim(0,8)
  plt.savefig(path+"_2.svg")
  plt.show()
  intervals=[[2,5],[2,3]]
  createExamplePlots(intervals,x,path+"_2","blue")
  plt.ylim(0,3)
  plt.xlim(0,8)
  plt.savefig(path+"_3.svg")
  plt.show()
  createExamplePlots(intervals,x,path+"_2","blue")
  colorEnvelope_2()
  plt.ylim(0,3)
  plt.xlim(0,8)
  plt.savefig(path+"_4.svg")
  plt.show()
  intervals = [[2,3]]
  createExamplePlots(intervals,x,path+"_3","blue")
  plt.ylim(0,3)
  plt.xlim(0,8)
  plt.savefig(path+"_5.svg")
  plt.show()
  createExamplePlots(intervals,x,path+"_3","blue")
  colorEnvelope_3()
  plt.ylim(0,3)
  plt.xlim(0,8)
  plt.savefig(path+"_6.svg")
  plt.show()
