\section{Algorithmen auf Persistenzlandschaften}
\label{sec:Algorithmen}
Um Persistenzlandschaften gewinnbringend zur Datenanalyse einzusetzen, benötigen wir effiziente Algorithmen zu ihrer Berechnung. In diesem Kapitel stellen wir jeweils einen Algorithmus zur Konstruktion, zur Berechnung von Linearkombinationen sowie zur Bestimmung der Norm von Persistenzlandschaften vor.

\subsection{Konstruktion von Persistenzlandschaften}
Zur Konstruktion von Persistenzlandschaften gehen wir davon aus, dass die persistente Homologie eines Datensatzes in Form von Persistenzintervallen $\{(b_i,d_i)\}_{i\in I}$ vorliegt. Die Aufgabe des Konstruktionsalgorithmus ist nun, aus diesen Intervallen eine Folge von Funktionen nach \cref{defLandscape} zu generieren. Die stückweise definierten Funktionen werden durch ihre kritischen Punkte repräsentiert, die wie folgt definiert werden.

\begin{defi}[Kritischer Punkt]
Die kritischen Punkte $(a,b)$ einer stückweise linearen Funktion $f$ sind genau die Punkte $(a,f(a))$, an denen sich die Definition von $f$ ändert, also für die es Intervalle $(\epsilon_1,a]$ und $(a,\epsilon_2]$ gibt, so dass:
\[
	f(x) =
	\begin{cases}
	f_1(x)		& \text{ für } x \in (\epsilon_1,a] \\
	f_2(x)		& \text{ für } x \in (a,\epsilon_2] \\
	\end{cases}
\]
Mit linearen Funktionen $f_1 \neq f_2$ und $\epsilon_1,\epsilon_2 \in \mathbb{R}$.
\end{defi}

%\begin{figure}
%\centering
%\includesvg[scale=0.5]{/home/jens/Uni/data_topology/Project/plots/landscapeSortExample}\\
%\caption{Die Überlagerung der Dreiecksfunktionen zu den Intervallen $ \{[1,6],[1,5],[2,7],[2,3] \}$.}
%\label{fig:superposition}
%\end{figure}

Anschaulich gehört zu jedem Persistenzintervall eine Dreiecksfunktion, siehe \cref{def:TriFunc}. Man kann sich daher die Persistenzlandschaft einer Menge von Intervallen als Überlagerung solcher Dreiecksfunktionen vorstellen (siehe \autoref{fig:superposition}). Um Abschnitte dieser stückweise definierten Funktionen den richtigen Ebenen zuzuweisen, bestimmt der hier vorgestellte Algorithmus zunächst alle Funktionsabschnitte, die zur äußeren Einhüllenden der überlagerten Dreiecksfunktionen gehören, setzt sie zu einer Funktion (der $k$-ten Ebene der Persistenzlandschaft) zusammen und entfernt diese Ebene aus dem Datensatz. Anschließend wird das Verfahren wiederholt, bis keine Dreiecksfunktionen mehr vorhanden sind. Dazu geht der hier vorgestellte Algorithmus aus \emph{"`A Persistence Landscapes Toolbox For Topological Statistics"'}~\cite{landscapeToolbox} folgendermaßen vor: \\\\

\begin{figure}
\newcommand{\subfigRatio}{0.45}
\centering
\begin{subfigure}{\subfigRatio\linewidth}
\centering
\includesvg[width=\textwidth]{/home/jens/Uni/data_topology/Project/plots/superpositionExample}
\end{subfigure}
%
\begin{subfigure}{\subfigRatio\linewidth}
\centering
\includesvg[width=\textwidth]{/home/jens/Uni/data_topology/Project/plots/superpositionExample_0}
\end{subfigure}
%
\begin{subfigure}{\subfigRatio\linewidth}
\centering
\includesvg[width=\textwidth]{/home/jens/Uni/data_topology/Project/plots/superpositionExample_1}
\end{subfigure}
%
\begin{subfigure}{\subfigRatio\linewidth}
\centering
\includesvg[width=\textwidth]{/home/jens/Uni/data_topology/Project/plots/superpositionExample_2}
\end{subfigure}
%
\begin{subfigure}{\subfigRatio\linewidth}
\centering
\includesvg[width=\textwidth]{/home/jens/Uni/data_topology/Project/plots/superpositionExample_3}
\end{subfigure}
%
\begin{subfigure}{\subfigRatio\linewidth}
\centering
\includesvg[width=\textwidth]{/home/jens/Uni/data_topology/Project/plots/superpositionExample_4}
\end{subfigure}
%
\begin{subfigure}{\subfigRatio\linewidth}
\centering
\includesvg[width=\textwidth]{/home/jens/Uni/data_topology/Project/plots/superpositionExample_5}
\end{subfigure}
%
\begin{subfigure}{\subfigRatio\linewidth}
\centering
\includesvg[width=\textwidth]{/home/jens/Uni/data_topology/Project/plots/superpositionExample_6}
\end{subfigure}
\caption{Der Algorithmus bestimmt schrittweise die obere Einhüllende und modifiziert die Liste der noch zu bearbeitenden Intervalle so, dass die Intervalle in der Liste die verbliebenen Dreiecksfunktionen repräsentieren.}
\label{fig:construction}
\end{figure}

\textbf{Schritt $1$:}
Zunächst werden alle Intervalle nach der folgenden Relation sortiert.
\begin{defi}
\label{def:defAlgoSort}
	Seien $I = [a,b]$ und ${I^\prime} = [a^\prime,b^\prime]$ Persistenzintervalle, dann ist $I < I'$, wenn gilt: 
	\begin{align}
			a &< a^\prime \text{ oder} \\
		 	a &= a^\prime \text{ und } b > b^\prime.
	\end{align}
\end{defi}

\textbf{Schritt 2:}
Um die äußerste Ebene der Persistenzlandschaft zu bestimmen, müssen alle kritischen Punkte der Ebene berechnet werden.
Die in \cref{def:defAlgoSort} angegebene Sortierung erlaubt die äußeren Punkte strukturiert auszuwählen, wobei einige Fälle unterschieden werden müssen. Wir betrachten jeweils das aktuelle Persistenzintervall $(b,d)$ und das nächste relevante Intervall $(b^\prime,d^\prime)$, d.h. das erste Intervall der Liste für welches gilt, dass $d^\prime \geq d$. Die Intervalle, für die $d^\prime < d$ gilt, sind nur für spätere Ebenen interessant, da die zugehörigen Dreiecksfunktionen vollständig vom Intervall $(b,d)$ umschlossen werden (siehe \autoref{fig:construction}). Nachdem ein Intervall abgearbeitet wurde, wird das nächste Intervall zum aktuellen und ein neues nächstes wird gesucht.

\begin{itemize}
\item \textbf{Falls $b^\prime > d$}, liegt der Beginn der nächsten relevanten Dreiecksfunktion hinter dem Ende der Aktuellen. Somit können wir schlicht den Endpunkt der aktuellen Funktion $(d,0)$ und den Startpunkt der nächsten $(b^\prime,0)$ hinzufügen. 
\item \textbf{Falls $b^\prime = d$}, liegt der Beginn der nächsten relevanten Dreiecksfunktion genau auf dem Ende der aktuellen. Hier fügen wir ebenfalls den entsprechenden Punkt $(d,0)$ hinzu.
\item \textbf{Falls $b^\prime < d$}, überschneiden sich die aktuelle und die nächste Dreiecksfunktion. In diesem Fall fügen wir den Schnittpunkt hinzu.
\end{itemize}

In jedem Fall wird die Spitze der aktuellen Dreiecksfunktion als kritischer Punkt hinzugefügt.\\

\textbf{Schritt $3$:} 
Nachdem die kritischen Punkte bestimmt wurden, muss die Liste der Persistenzintervalle für die Berechnung der nächsten Ebene so vorbereitet werden, dass sie die Dreieckfunktionen ohne die oberste Ebene repräsentiert (siehe \autoref{fig:construction}). Dazu müssen wir wieder zwischen Intervallpaaren mit und ohne Überschneidungen unterscheiden.

\begin{itemize}
\item \textbf{Falls $b^\prime \geq d$}, gibt es keine Überschneidungen. Somit kann das aktuelle Intervall schlicht entfernt werden.
\item \textbf{Falls $b^\prime < d$}, überschneiden sich das aktuelle und das nächste Intervall. In diesem Fall entfernen wir das aktuelle Intervall und fügen ein neues Intervall $(b^\prime,d)$ entsprechend der Sortierung aus Schritt $1$ hinzu, um die Teile der Dreieckfunktionen zu repräsentieren, die nicht zur äußeren Ebene gehören (siehe \cref{fig:construction}).
\end{itemize}

Im eigentlichen Algorithmus werden Schritt $1$ und $2$ jeweils für das gerade betrachtete Intervallpaar ausgeführt. Die Komplexität der Konstruktion ist damit linear in der Anzahl der kritischen Punkte $k$, wobei für den Fall, dass sich tatsächlich alle Dreiecksfunktionen der $n$ Persistenzintervalle überschneiden, gilt: $k = n^2 + 2n = \mathcal{O}(n^2)$, wie Dłotko und Bubenik 2015 gezeigt haben~\cite{landscapeToolbox}. Insgesamt ist die Konstruktion einer Persistenzlandschaft also quadratisch in der Anzahl der Persistenzintervalle.

\begin{algorithm}
		\renewcommand{\algorithmicrequire}{\textbf{Input:}}
		\renewcommand{\algorithmicensure}{\textbf{Output:}}
		\caption{Berechnung der Persistenzlandschaft}
	\begin{algorithmic}[1]
		\Require Eine Liste von Persistenzintervallen $A = \{[b_i,d_i]\}_{i=1}^n$, mit $-\infty \le b_i \le d_i \le \inf$. 
		\Ensure{Eine Liste der kritischen Punkte $\{ \mathbb{L}_k\}$}.
		\State Sortiere A in erster Ordnung nach aufsteigendem $b$ und in zweiter Ordnung nach absteigendem d.
		\State k = 1;
		\While{$A \neq \emptyset$}
			\State $\mathbb{L}_k = \{\}$;
			%\State $(b,d) = $ A.pop$()$ %Hole das Erste Element $(b,d)$ von $A$;
			%\State $p = $ A.front$()$ %Richte $p$ auf das nächste Element; 
			\State Hole das Erste Element $(b,d)$ von $A$;
			\State Richte $p$ auf das nächste Element;
			\State Füge $((-\infty, 0), (b,0), (\frac{b+d}{2}, \frac{b-d}{2}))$ zu $\mathbb{L}_k$ hinzu;
			\While{last($\mathbb{L}_k$) $ \neq (\infty,0)$}
				\If{$d$ maximal unter den, ab $p$, in $A$ verbleibenden Elementen}
				%\If {$d = \max(\{A_p,A_{p+1},\dots,A_n\})$}
					\State Füge $(d,0), (\infty,0)$ zu $\mathbb{L}_k$ hinzu;
				\Else
					%$(b', d') = p$
					%\While {$d' < d$}
					%	\State $(b', d') = (b', d')$.next()
					\State Sei $(b', d')$ das erste Element von $A$ ab $p$, mit $d' \ge d$;
					%\State Hole $(b', d')$ von A;
					%\EndWhile
					%\State $p = (b', d')$.next$()$ %
					\State Richte $p$ auf das nächste Element;
					%\State $bla $
					\If{$b' > d$}
						\State Füge $(d,0)$ zu $\mathbb{L}_k$ hinzu;
					\EndIf
					\If{$b' \geq d$}
						\State Füge $(b',0)$ zu $\mathbb{L}_k$ hinzu;
					\Else
						\State Füge $(\frac{b' + d}{2}, \frac{d - b'}{2})$ zu $\mathbb{L}_k$ hinzu;
						\State Füge $(b',d)$ nach der oben definierten Ordnung in A ein;
						\State Richte $p$ auf das nächste Element;
					\EndIf
					\State Füge $(\frac{b' + d'}{2}, \frac{d' - b'}{2})$ zu $\mathbb{L}_k$ hinzu;
					\State $(b,d) = (b',d')$;
				\EndIf
			\EndWhile
			\State ++k;
		\EndWhile

	\end{algorithmic}
	\end{algorithm}

\subsection{Linearkombinationen von Persistenzlandschaften}
Nach \cref{def:LinComb} fassen wir die Linearkombination von Persistenzlandschaften schlicht als Linearkombination von stückweise linearen Funktionen auf. In diesem Abschnitt wird ein kurzer Algorithmus zur Berechnung dieser Linearkombinationen vorgestellt, der auf der Repräsentation von Persistenzlandschaften durch ihre kritischen Punkte aufbaut. Im Folgenden fassen wir Persistenzlandschaften $\lambda$ als die Folge ihrer Ebenen $\lambda = (\lambda_k)_{k=1,\dots,M}$ auf, wobei $M$ die Anzahl der nicht trivialen Ebenen bezeichnet. Mehrere Persistenzlandschaften $\lambda^1,\lambda^2$ werden durch einen oberen Index unterschieden.

\begin{algorithm}
\renewcommand{\algorithmicrequire}{\textbf{Input:}}
\renewcommand{\algorithmicensure}{\textbf{Output:}}
\caption{Linearkombination von Persistenzlandschaften~\cite{landscapeToolbox}}
\label{algoLinComb}
\begin{algorithmic}
\Require{ Eine Menge von Persistenzlandschaften $\{(\lambda_k^1)_{k=1,\dots,M},\dots,(\lambda_k^N)_{k=1,\dots,M}\}$, gegeben durch ihre kritischen Punkte $((\mathbb{X}_k^1,\mathbb{Y}_k^1), \dots, (\mathbb{X}_k^N,\mathbb{Y}_k^N))_{k=1,\dots,M}$ und Gewichte bzgl. der Linearkombination $a_1,\dots,a_N$.}
\Ensure{ Eine Menge von kritischen Punkten, die die resultierende Landschaft definieren $((\mathbb{X}_k,\mathbb{Y}_k))_{k=1,\dots,M}$.}
\For {$k = 1,\dots,M$}
	\State $\mathbb{X}_k = \text{unique}(\text{sort}(\bigcup_{i=1,\dots,N} \mathbb{X}_k^i))$
	\For {$j = 1,\dots,N$}
		 \State $\bar{\mathbb{Y}}_k^j = \bar{\lambda}_k^j(\mathbb{X}_k)$
	\EndFor
	\State $\mathbb{Y}_k = \sum_{j=1}^{N}a_j \bar{\mathbb{Y}}_k^j$
\EndFor
\State $\mathbb{X} = (\mathbb{X}_k)_{k=1,\dots,M}$
\State $\mathbb{Y} = (\mathbb{Y}_k)_{k=1,\dots,M}$\\
\Return $(\mathbb{X},\mathbb{Y})$
\end{algorithmic}
\end{algorithm}

Der Algorithmus behandelt jede Ebene $k$ einzeln, wobei die Linearkombination der $k$-ten Ebenen der Persistenzlandschaften jeweils gemäß \cref{def:LinComb} aus der punktweisen gewichteten Summe der einzelnen Landschaften besteht. Die resultierende Landschaft wird wiederum durch ihre kritischen Punkte repräsentiert. Diese ergeben sich aus der Vereinigung der kritischen Punkte aller Einzellandschaften. Für jeden $x$-Wert eines kritischen Punktes muss der entsprechende $y$-Wert in allen Landschaften bestimmt werden. Anschließend werden die $y$-Werte der resultierenden Persistenzlandschaft durch die gewichtete Summe der vorher bestimmten Werte berechnet, wie in \cref{algoLinComb} beschrieben. \\

Wir wollen nun die Komplexität von \cref{algoLinComb} bestimmen. Man beachte, dass die Konstruktion jeder der Persistenzlandschaften $\lambda^j$ auf eine Anzahl von kritischen Punkten $n^j$ zurückgeht.
Sei $n:= \max_j(n^j)$. Mit $p_k^j$ bezeichnen wir die Anzahl der kritischen Punkte von $\lambda^j$ in Ebene $k$. Wir definieren: 
$$P_k := \sum_{j=1}^N p_k^j.$$ 
Dies ist damit die Anzahl aller kritischen Punkte der Ebene $k$ aller $N$ Landschaften. Man beachte, dass $P_k$ = $\mathcal{O}(nN)$ gilt. In der $k$-ten Ebene muss also jede der $N$ Landschaften an allen $P_k$ Punkten ausgewertet werden. Pro Ebene erhalten wir damit eine Komplexität von $\mathcal{O}(N P_k) = \mathcal{O}(nN^2)$. Um die Persistenzlandschaft auf allen Ebenen $k$ zu bestimmen, muss jede Landschaft auf allen kritischen Punkten aller $N$ Landschaften ausgewertet werden. Ist $P$ die Anzahl aller kritischen Punkte, so sind also $\mathcal{O}(PN)$ Auswertungen notwendig. Wir wissen bereits, dass für jede Landschaft $P^j=\mathcal{O}(n^2)$ Punkte vorhanden sind. Insgesamt gilt also: $P=\mathcal{O}(Nn^2)$. Für die Laufzeit von \cref{algoLinComb} ergibt sich somit: $$\mathcal{O}(PN)=\mathcal{O}(n^2N^2)\text{~\cite{landscapeToolbox}.} $$
%Dabei .  so ergeben sich $\mathcal{O}(N P_k)$ Auswertungen und %(im Gegensatz zu der Behauptung des persistence toolbox papers) 
Die Komplexität dieser Berechnung lässt sich erheblich reduzieren, wenn man die Lage der kritischen Punkte auf ein Gitter beschränkt~\cite{landscapeToolbox}. Diese Optimierung ist für Auswertungen auf großen Datenmengen empfehlenswert, wenn auf den gegebenen Daten nicht zu viele Informationen verloren gehen. Da in dieser Arbeit jedoch die prinzipielle Eignung von Persistenzlandschaften zur Datenanalyse gezeigt werden soll, wird an dieser Stelle nicht weiter darauf eingegangen. %Man beachte, dass jede naive Auswertung in einer Ebene einer Persistenzlandschaft $\mathcal{O}(\log p_k)$ Operationen benötigt, um den jeweiligen Funktionsabschnitt zu finden. 

\subsection{Normen auf Persistenzlandschaften}

Wie in \cref{def:defLanNorm} bereits ausgeführt, ist es möglich, die Berechnung der Norm einer Persistenzlandschaft auf eine geschlossene Formel zurückzuführen. Ein Algorithmus, der die Norm einer Persistenzlandschaft bestimmt, muss demnach nur die Landschaft in ihre linearen Abschnitte unterteilen und gegebenenfalls die Funktionsabschnitte in ihre negativen und positiven Anteile zerlegen.
Es ergibt sich also folgender Algorithmus:

\begin{algorithm}
\renewcommand{\algorithmicrequire}{\textbf{Input:}}
\renewcommand{\algorithmicensure}{\textbf{Output:}}
\caption{Norm einer Persistenzlandschaft}
\label{alg:algoNorm}
\begin{algorithmic}
\Require {Eine Persistenzlandschaft $\lambda = \{\lambda_1,\dots, \lambda_k\}$ sowie eine natürliche Zahl $p$.}
\Ensure {Die $p$-Norm der Persistenzlandschaft $\lambda$.}
\State Initialisiere $\vert\vert \lambda \vert\vert = 0$.
\For {$i = 1,\dots,k$}
	\State Seien $C_0,\dots,C_m$ die kritischen Punkte der $k$-ten Ebene. 
	\For {$j=1,\dots,m$}
		\State{Berechne die Norm des positiven und des negativen Anteils der Landschaft zwischen} 
		\State {$C_j$ und $C_{j-1}$ $\vert\vert \lambda_i^+\vert_{C_{j-1}}^{C_j} \vert\vert$, $\vert\vert \lambda_i^-\vert_{C_{j-1}}^{C_j} \vert\vert$}
		\State {$\vert\vert \lambda \vert\vert += \vert\vert \lambda_i^+\vert_{C_{j-1}}^{C_j} \vert\vert + \vert\vert \lambda_i^-\vert_{C_{j-1}}^{C_j} \vert\vert$}
	\EndFor
\EndFor\\
\Return $\vert\vert \lambda \vert\vert$
\end{algorithmic}
\end{algorithm}

\subsection{Unendliche Persistenz}
\label{inftPers}
Am Anfang des Kapitels sind wir davon ausgegangen, dass die persistente Homologie immer in Form einer Sequenz von endlichen Persistenzintervallen vorliegt. Diese Bedingung kann jedoch aus verschiedenen Gründen unerfüllt bleiben. Insbesondere bei der Betrachtung von persistenter Homologie auf Graphen werden wir einige Intervalle mit unendlicher Persistenz, d.h. der Form $(b,\infty)$ erhalten, die in der Terminologie der Persistenzdiagramme auch "`\emph{unpaired points}"' genannt werden. Es ist gängige Praxis, den theoretisch unendlichen Wert dieser Punkte auf einen im Vergleich zu den anderen oberen Intervallgrenzen großen Wert zu setzen. Das ermöglicht, die entsprechenden Punkte im Rahmen weiterer Analyse zu verarbeiten und trotzdem eine Unterscheidbarkeit zwischen endlichen und unendlichen Punkten zu erhalten. Es stellt sich allerdings die Frage, wie groß "`groß"' eigentlich sein soll. Als Programmierer ist man versucht, einen großen Wert schlicht durch die maximale, durch den entsprechenden Datentyp darstellbare Zahl zu repräsentieren. Eine solche Wahl macht die Weiterverarbeitung jedoch oft schwierig, da man den Wert hinterher nicht mehr ohne numerische Probleme mit anderen, möglicherweise ebenfalls sehr großen, Werten addieren oder multiplizieren kann.
Wählt man dagegen einen beliebigen großen Wert, riskiert man, dass die Skala eines gegebenen Datensatzes über den entsprechenden Wert hinausgeht, und man auch endliche Punkte abschneidet und die Information des Persistenzdiagramms verfälscht. Dieser Ansatz ist vor allem dann sinnvoll, wenn man aus einem bestimmten Grund annehmen kann, dass die endlichen Persistenzintervalle nicht über einen bestimmten Punkt hinausgehen können. Eine sinnvolle obere Grenze könnte beispielsweise der maximale Abstand zwischen zwei Datenpunkten bei einem Datensatz aus Punkten im $\mathbb{R}^n$ sein.
Eine andere Möglichkeit ist die Persistenzintervalle bei einem Vielfachen des maximalen vorkommenden endlichen Wertes abzuschneiden. 
Die Implementierung lässt sowohl eine vom Nutzer definierte obere Grenze, als auch ein automatisches Abschneiden zu, wobei der Anwender die Entscheidung hier immer bewusst, mit Blick auf die betrachteten Daten treffen sollte.

\subsection{Effizienz von Persistenzlandschaften}
\label{subsec:performance}
%\subsubsection{Synthetischer Benchmark auf zufälligen Persistenzdiagrammen}
%\label{subsec:performanceRNG}
Im folgenden Kapitel zeigen wir, dass unterschiedliche geometrische und topologische Daten sich anhand ihrer Persistenzlandschaften unterscheiden lassen. Zu diesem Zweck bestimmen wir die $L_2$ Distanzen von Persistenzlandschaften. 
Um zu zeigen, dass die Verwendung von Distanzen von Persistenzlandschaften gegenüber der Bestimmung von Distanzen von Wassersteinmetriken nicht nur aussagekräftige Ergebnisse liefert, sondern, dass unsere Implementierung auch entscheidende Vorteile in der Rechenzeit bietet, haben wir die Distanzberechnung anhand von jeweils zwei zufällig gewählten Persistenzdiagrammen mit $100$ bis $1000$ zufälligen Persistenzpunkten jeweils mithilfe der Wassersteindistanz sowie der Distanz der Persistenzlandschaften bestimmt (siehe \autoref{fig:landscPerformance}). 
Die Ergebnisse zeigen sehr deutlich, dass die Berechnungen von Distanzen von Persistenzlandschaften bereits bei wenigen Persistenzpunkten schneller ist als die Berechnung der Wassersteindistanzen. Erst ab $500$ Punkten wird eine von unserem Test erfasste Laufzeit von $0.01$ Minuten gemessen, während die Auswertung der Wassersteindistanz hier bereits bei ca. $11$ Minuten liegt. In \cref{fig:performancePlot} wird deutlich, dass die Komplexität der Wassersteindistanz in der Praxis erheblich schlechter skaliert als die Persistenzlandschaft.
\begin{figure}
\centering
\begin{tabular}{ l l l }
\toprule
Anzahl der Features & Landschaften-Distanz & Wasserstein-Distanz \\
\midrule
100 & 0.00 m& 0.03 m \\ 
200 & 0.00 m& 0.32 m \\
300 & 0.00 m& 1.55 m \\
400 & 0.00 m& 3.86 m \\
500 & 0.01 m& 11.01 m\\
600 & 0.01 m& 23.71 m\\
700 & 0.03 m& 35.32 m\\
800 & 0.05 m&100.10 m\\
900 & 0.05 m&155.29 m\\
1000& 0.07 m&178.27 m\\
\bottomrule
\end{tabular}
\caption{Performanz der Distanzen von Persistenzlandschaften gegenüber Wassersteindistanzen.}
\label{fig:landscPerformance}
\end{figure}

\begin{figure}
\includesvg[width=\linewidth]{/home/jens/Uni/data_topology/Project/plots/performance_plot.svg}
\caption{Distanzberechnungen von Persistenzlandschaften sind auch schon für kleine Beispiele deutlich effizienter als Wassersteindistanzen.}
\label{fig:performancePlot}
\end{figure}

%\subsubsection{Performanz auf Netzwerken}
%\label{subsec:performanceGraph}
%Der Test in \cref{subsec:performanceRNG} ist sehr generisch und eignet sich daher gut um das asymptotische Verhalten der Methoden zu vergleichen. Da der tatsächliche Rechenaufwand beider Methoden in unterschiedlicher Art und Weise davon abhängt, wie die Persistenzpunkte verteilt sind, ergibt er jedoch kein besonders realistisches Bild der tatsächlich zu erwartenden Performanz. Um dies zu analysieren haben wir die Laufzeit bei der Berechnung von Distanzen synthetischer Netzwerke (siehe \cref{sec:PHofNetworks}) ausgewertet. Man beachte, dass die genaue Anzahl der kritischen Punkte stark vom jeweiligen Modell abhängt, daher lassen sich die einzelnen Experimente nur schwer miteinander vergleichen. Es wir jedoch klar, dass die Auswertungen anhand von Persistenzlandschaften auch hier weniger deutlich effizienter sind. Alle Benchmarks wurden auf einem Intel(R) Core(TM) i5-3320M CPU @ 2.60GHz, also einem gewöhnlichen Mittelklasse Prozessor älterer Generation durchgeführt und in CPU-Minuten auf die zweite Stelle gerundet angegeben.
%Wir stellen damit fest, dass die aus der Theorie erwarteten Laufzeitverbesserungen sich auch tatsächlich nachweisen lassen. Analysen durch Persistenzlandschaften können also vom Standpunkt der Effizienz aus als gute Alternative zu herkömmlichen Methoden auf Basis von Persistenzdiagrammen betrachtet werden. Die folgenden Kapitel werden sich nun mit der Frage auseinandersetzen, ob Persistenzlandschaften auch in der Lage sind die Persistente Homologie unterschiedlicher Daten akkurat zu beschreiben.
%\begin{figure}
%\begin{tabular}{l l l}
%Getestete Netzwerkmodelle & Landschaften Distanz & Wasserstein-Distanz \\
%barabasi-albert -- $G_{nm}$ & 0.01 m & 22.41 m \\
%gnp(0.5) -- gnp(0.7) & & 3.29 m
%\end{tabular}
%\end{figure}
