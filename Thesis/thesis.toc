\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {ngerman}
\contentsline {section}{\numberline {1}Einf\IeC {\"u}hrung}{7}{section.1}
\contentsline {section}{\numberline {2}Einbettung in den wissenschaftlichen Kontext}{9}{section.2}
\contentsline {section}{\numberline {3}Mathematische Grundlagen}{12}{section.3}
\contentsline {subsection}{\numberline {3.1}Algebraische Topologie}{12}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Simpliziale Homologie}{14}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Berechnung simplizialer Homologie}{20}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Persistente Homologie}{20}{subsection.3.4}
\contentsline {section}{\numberline {4}Algorithmen auf Persistenzlandschaften}{30}{section.4}
\contentsline {subsection}{\numberline {4.1}Konstruktion von Persistenzlandschaften}{30}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Linearkombinationen von Persistenzlandschaften}{34}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}Normen auf Persistenzlandschaften}{35}{subsection.4.3}
\contentsline {subsection}{\numberline {4.4}Unendliche Persistenz}{35}{subsection.4.4}
\contentsline {subsection}{\numberline {4.5}Effizienz von Persistenzlandschaften}{37}{subsection.4.5}
\contentsline {section}{\numberline {5}Persistenzlandschaften auf synthetischen Daten}{39}{section.5}
\contentsline {subsection}{\numberline {5.1}Torus und Sph\IeC {\"a}re}{39}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Fazit}{44}{subsection.5.2}
\contentsline {section}{\numberline {6}Persistente Homologie von Netzwerken}{45}{section.6}
\contentsline {subsection}{\numberline {6.1}Berechnung persistenter Homologie auf Netzwerken}{45}{subsection.6.1}
\contentsline {subsubsection}{\numberline {6.1.1}Persistente Homologie in Dimension 0}{48}{subsubsection.6.1.1}
\contentsline {subsubsection}{\numberline {6.1.2}Persistente Homologie in Dimension 1}{48}{subsubsection.6.1.2}
\contentsline {subsubsection}{\numberline {6.1.3}Ausdruckskraft der Grad-Filtration}{49}{subsubsection.6.1.3}
\contentsline {subsection}{\numberline {6.2}Persistenzlandschaften von Netzwerkmodellen}{50}{subsection.6.2}
\contentsline {subsubsection}{\numberline {6.2.1}Netzwerkmodelle}{51}{subsubsection.6.2.1}
\contentsline {subsubsection}{\numberline {6.2.2}Analyse der persistenten Homologie von Netzwerken}{52}{subsubsection.6.2.2}
\contentsline {subsection}{\numberline {6.3}Mittlere Persistenzlandschaften}{54}{subsection.6.3}
\contentsline {subsubsection}{\numberline {6.3.1}Vergleich: $G_{np}(0.07)$ Netzwerke - $G_{np}(0.05)$ Netzwerke}{56}{subsubsection.6.3.1}
\contentsline {subsubsection}{\numberline {6.3.2}Vergleich: $G_{np}$-Netzwerke - $G_{nm}$-Netzwerke}{58}{subsubsection.6.3.2}
\contentsline {subsubsection}{\numberline {6.3.3}Vergleich: $G_{nm}$-Netzwerke - Barabasi-Albert-Netzwerke}{58}{subsubsection.6.3.3}
\contentsline {subsection}{\numberline {6.4}Persistente Homologie von Graphen mit gemeinsamer Grad-Sequenz}{59}{subsection.6.4}
\contentsline {subsubsection}{\numberline {6.4.1}Netzwerkmodelle mit fester Grad-Sequenz}{59}{subsubsection.6.4.1}
\contentsline {subsubsection}{\numberline {6.4.2}Distanzen von Graphen mit fester Grad-Sequenz}{60}{subsubsection.6.4.2}
\contentsline {subsection}{\numberline {6.5}Kernel-PCA auf synthetischen Daten}{60}{subsection.6.5}
\contentsline {subsubsection}{\numberline {6.5.1}Grundlagen}{61}{subsubsection.6.5.1}
\contentsline {subsubsection}{\numberline {6.5.2}Einbettung von Netzwerken}{63}{subsubsection.6.5.2}
\contentsline {subsubsection}{\numberline {6.5.3}Fazit}{63}{subsubsection.6.5.3}
\contentsline {section}{\numberline {7}Berechnung von Persistenzlandschaften auf Reddit-Graphen}{66}{section.7}
\contentsline {subsection}{\numberline {7.1}Die Daten}{66}{subsection.7.1}
\contentsline {subsection}{\numberline {7.2}Distanzmatrizen der Reddit Graphen}{66}{subsection.7.2}
\contentsline {subsection}{\numberline {7.3}Kernel-PCA der Reddit-Graphen}{67}{subsection.7.3}
\contentsline {subsection}{\numberline {7.4}Voraussagen mithilfe von Support Vektor Maschinen}{69}{subsection.7.4}
\contentsline {subsection}{\numberline {7.5}Fazit}{70}{subsection.7.5}
\contentsline {section}{\numberline {8}Fazit \& Ausblick}{71}{section.8}
\contentsline {paragraph}{\nonumberline Zusammenfassung}{73}{section*.42}
