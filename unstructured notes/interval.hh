#include <iostream>

template <typename T = double>
class Interval {

  using Borders = std::pair<T,T>;
  
private:
  Borders b;
  
public:
  T upper();
  T lower();
  bool operator< (Interval<T,T> other);
  bool operator<= (Interval<T,T> other);
  bool operator> (Interval<T,T> other);
  bool operator>= (Interval<T,T> other);
  
  bool operator< (T other);
  bool operator> (T other);
}

template <typename T>
bool Interval<T>::operator< (Interval<T> other) 
{
  return (this->upper() < other->lower()) ? true : false;
}

template <typename T>
bool Interval<T>::operator<= (Interval<T> other) 
{
  return (this->upper() <= other->lower()) ? true : false;
}

template <typename T>
bool Interval<T>::operator> (Interval<T> other) 
{
  return (this->lower() > other->upper()) ? true : false;
}

template <typename T>
bool Interval<T>::operator>= (Interval<T> other) 
{
  return (this->lower() >= other->upper()) ? true : false;
}

template <typename T>
bool Interval<T>::operator< (T other)
{
  return (this->upper < other ) ? true : false;
}

template <typename T>
bool Interval<T>::operator> (T other) 
{
  return (this->lower > other) ? true : false;
}
