#include <vector>
#include <algorithm>
#include "tools.hh"
#include "persistence_landscape.hh"
#include "global.hh"

#ifndef PL_H_
#define PL_H_

namespace pl {
/* Non class functions of the namespace pl.
 * - linComb_singleLayer (XY, a): calculate the linear combination of several 
 *   persistence landscapes on a fixed layer. 
 * - linComb (
 */

  /* 
   * 
   */
  template<typename T, typename R, typename S>
  std::pair<std::vector<T>, std::vector<R> > linComb_singleLayer ( const std::vector<PersistenceLandscape<T,R,S>>& landscapeV, const std::vector<S>& a,const size_t k)
  {
    size_t N = landscapeV.size();
    // merge the vectors evtl. with std::accumulate TODO: capsule this in a structure
    std::vector<T> X;
    for (auto landscape : landscapeV)
    {
      std::vector<T> tmp = {};
      std::vector<T> X_i = landscape.getX()[k];
      std::merge(X.begin(), X.end(), X_i.begin(), X_i.end(), std::back_inserter(tmp) );
      X = tmp;
    }
    // erase duplicates
    std::unique(X.begin(), X.end(), [](const double& a, const double& b) { return tools::equals(a,b); } );
    // calculate stuff
    std::vector<std::vector<R> > Y_;
    for (size_t j = 0; j < N; j++)
    {
      Y_.push_back(std::vector<R>() );
      for (auto x_i : X )
      {
        Y_[j].push_back( landscapeV[j](k, x_i) ); 
      }
    }
    // encapsule this in tools. accumulate or something
    std::vector<double> Y(X.size(),0); // = std::accumulate(Y_.begin(), Y_.end(), std::vector<double>(X.size(), 0), [a] (std::vector<double> Y1, std::vector<double> Y2) { return Y1 + a[j]  * Y2
    for (size_t j = 0; j < N; j++)
    { 
      { 
        using namespace global;
        Y += (a[j] * Y_[j]);
      }
    }
    
    return std::make_pair(X,Y);
  }
  
  // works for equal layer counts only (for now) TODO: implement adding trivial 0 pairs for non equal sized landscapes
  template <typename T, typename R, typename S>
  PersistenceLandscape<T,R,S> linComb (std::vector<PersistenceLandscape<T,R,S>> landscapeV, std::vector<S> scalarV)
  {
    size_t layerN = landscapeV[0].layer();
    std::vector<std::vector<T>> X;
    std::vector<std::vector<R>> Y;
    for(size_t k = 0; k < layerN; k++)
    {
      std::vector<std::pair< std::vector<T>, std::vector<R> >> inputV;
      for (auto landscape : landscapeV)
      {
        assert (landscape.layer() == layerN);
        //inputV.push_back(std::pair< std::vector<T>, std::vector<R> >(landscape.getX()[k], landscape.getY()[k]) );
        auto result = linComb_singleLayer(landscapeV, scalarV, k);
        X.push_back(result.first);
        Y.push_back(result.second);
      }
    }
    return PersistenceLandscape<T,R,S>(X, Y);
  }
}

#endif // PL_H_
