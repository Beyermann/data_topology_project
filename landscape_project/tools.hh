#include <cmath>
#include <vector>
#include <boost/icl/continuous_interval.hpp>

#ifndef TOOLS_H_
#define TOOLS_H_

namespace tools {

template <typename T = double>
using Interval = boost::icl::continuous_interval<T>;
  
  bool equals (const double& a, const double& b, double precision = 1e-08)
  {
    return a == b;
    //return ( std::abs(a - b ) < precision);
  }

  struct intervalCompare
  {
    inline bool operator() (const Interval<>& a, const Interval<>& b)
    {
      // its important to evaluate this first, because of the special nature of float equality
      // but this solution is not very elegant, because we have to evaluate the equality term 
      // for all cases, while in most of them it will be unneccessary
      // TODO: Ask for a better solution
      if ( (equals(lower(a), lower(b))) && (upper(a) > upper(b)) ) 
      {
        return true;
      }
      else if (lower(a) < lower(b))
      {
        return true;
      }
      else
      {
        return false;
      }
    }
  };
  
  template<typename T>
  std::pair< std::vector<std::vector<T>>, std::vector<std::vector<T>> > cast_pair(std::vector<std::vector<std::pair<T,T> >> input)
  {
    std::vector<std::vector<T>> X;
    std::vector<std::vector<T>> Y;
    for (size_t k = 0; k < input.size(); k++)
    {
      X.push_back(std::vector<T>() );
      Y.push_back(std::vector<T>() );
      
      for (auto critPoint : input[k])
      {
        X[k].push_back(critPoint.first);
        Y[k].push_back(critPoint.second);
      }
    }
    return std::make_pair(X,Y);
  }

}


#endif // TOOLS_H_
