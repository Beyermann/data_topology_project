
#ifndef GLOBAL_H_
#define GLOBAL_H_

namespace aleph
{
namespace utilities
{
  
  template<typename T, typename R, typename S>
  std::ostream& operator<< ( std::ostream& os, std::vector<std::vector<std::pair<T,R>> > L)
  {
    for (auto Lk: L)
      {
        for (auto it : Lk)
        {
          os << "{"<<it.first<<", "<< it.second<< "}" << " ";
        }
        os << std::endl;
      }
      return os;
  }
  
  template <typename T, typename S>
  std::vector<T> operator* (const S& scalar, const std::vector<T>& vector)
  {
    std::vector<T> result;
    for (auto x : vector)
    {
      result.push_back(scalar * x);
    }
    return result;
  }

  template <typename T>
  std::vector<T> operator+ (const std::vector<T>& vector1, const std::vector<T>& vector2)
  {
    std::vector<T> result;
    size_t N = std::min(vector1.size(), vector2.size());
    for (size_t i = 0; i < N; i++)
    {
      result.push_back(vector1[i] + vector2[i]);
    }
    return result;
  }
  
  template <typename T>
  std::vector<T>& operator+= (std::vector<T>& vector1, const std::vector<T>& vector2)
  {
    assert(vector1.size() == vector2.size());
    size_t N = std::min(vector1.size(), vector2.size());
    for (size_t i = 0; i < N; i++)
    {
      vector1[i] += vector2[i];
    }
    return vector1;
  }
  
  template<typename T>
  std::pair< std::vector<std::vector<T>>, std::vector<std::vector<T>> > cast_pair(std::vector<std::vector<std::pair<T,T> >> input)
  {
    std::vector<std::vector<T>> X;
    std::vector<std::vector<T>> Y;
    for (size_t k = 0; k < input.size(); k++)
    {
      X.push_back(std::vector<T>() );
      Y.push_back(std::vector<T>() );
      
      for (auto critPoint : input[k])
      {
        X[k].push_back(critPoint.first);
        Y[k].push_back(critPoint.second);
      }
    }
    return std::make_pair(X,Y);
  }
}
}
#endif
