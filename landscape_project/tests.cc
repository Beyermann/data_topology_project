#include "tools.hh"
#include <cmath>
#include "persistence_landscape_test.hh"
#include <deque>
#include <iostream>
#include <limits>

template<typename T = double>
using Interval = boost::icl::continuous_interval<T>;
double inf = std::numeric_limits<double>::infinity();

template<typename T>
int test_cast_pair()
{
  using pair = std::pair<T,T>;
  std::vector<std::vector<std::pair<T,T> >> input = { std::vector<std::pair<T,T> >( {pair( 1, 2), pair( 3, 4), pair( 5, 6)} ), 
                                                      std::vector<std::pair<T,T> >( {pair( 7, 8), pair( 9,10), pair(11,12)} ),
                                                      std::vector<std::pair<T,T> >( {pair(13,14), pair(15,16), pair(17,18)} )};
  std::pair<std::vector<std::vector<T>>, std::vector<std::vector<T>> > result = tools::cast_pair(input);
  assert ( result == (std::pair<std::vector<std::vector<T>>, std::vector<std::vector<T>> > ( std::vector<std::vector<T> > ({ {1,3,5}, {7,9,11}, {13,15,17} }), 
                                                                                             std::vector<std::vector<T> > ({ {2,4,6}, {8,10,12}, {14,16,18} }) ) ) );
  return 0;
}

int test_landscape()
{
  //using namespace global;
  
  std::deque<Interval<>> data_1 = {Interval<>(1. , 5.), Interval<>(2. , 8.), Interval<>(3. , 4.), Interval<>(5. , 9.), Interval<>(6. , 7.), Interval<>(9.,10.) };
  std::deque<Interval<>> data_2 = {Interval<>(1. , 6.), Interval<>(2. , 3.), Interval<>(3. , 7.), Interval<>(4. , 5.) };

  pl::PersistenceLandscape<> l1(data_1);
  pl::PersistenceLandscape<> l2(data_2);
  
  for (auto layer : l1.getX())
  {
    std::cout << "{ ";
    for (auto el : layer)
    {
      std::cout << el << " ";
    }
    std::cout << "}" << std::endl;
  }
  assert( l1.getX() == std::vector<std::vector<double>>({{-inf, 1.0, 3.0, 3.5, 5.0, 6.5, 7.0, 9.0, 9.5, 10, inf}, 
                                                         {-inf, 2.0, 3.5, 5.0, 6.5, 8.0, inf},
                                                         {-inf, 3.0, 3.5, 4.0, 6.0, 6.5, 7.0, inf}} ) );
  
  assert( l1.getY() == std::vector<std::vector<double>>({{ 0.0, 0.0, 2.0, 1.5, 3.0, 1.5, 2.0, 0.0, 0.5, 0, 0.0 }, 
                                                         { 0.0, 0.0, 1.5, 0.0, 1.5, 0.0, 0.0 },
                                                         { 0.0, 0.0, 0.5, 0.0, 0.0, 0.5, 0.0, 0.0 }} ) );
  

  assert( l1(0,0.0) == 0); // test x-value smaller than first critical point
  assert( l1(0,1.0) == 0); // test x-value at first critical point
  assert( l1(0,2.0) == 1); // test x-value after first critical point
  assert( l1(0,1.001) >  0); // test x-value short after critical point
  assert( l1(0,4.0) == 2); // test x-value between critical points
  assert( l1(0,200) == 0); // test x-value larger than last critical point
  
  assert( l1(0,0.0) == l1[0](0) );
  assert( l1(0,1.0) == l1[0](1.0));
  assert( l1(0,2.0) == l1[0](2.0));  
  assert( l1(0,1.001) == l1[0](1.001));
  assert( l1(0,4.0) == l1[0](4.0));
  assert( l1(0,200) == l1[0](200));
  
  // TODO: ergebnisse ausrechnen und korrekte asserts einfügen 
  l1 + l2;
  l1 * 3.0;
  l2 += l1;
  l2 *= 3.0;
  
  
  
  /*
  {-inf, 0} {1, 0} {3, 2} {3.5, 1.5} {5, 3} {6.5, 1.5} {7, 2} {9, 0} {9.5, 0.5} {10, 0} {inf, 0} 
  {-inf, 0} {2, 0} {3.5, 1.5} {5, 0} {6.5, 1.5} {8, 0} {inf, 0} 
  {-inf, 0} {3, 0} {3.5, 0.5} {6, 0} {4, 0} {6.5, 0.5} {7, 0} {inf, 0}
  
  {-inf, 0} {1, 0} {3.5, 2.5} {4.5, 1.5} {5, 2} {7, 0} {inf, 0} 
  {-inf, 0} {2, 0} {2.5, 0.5} {3, 0} {4.5, 1.5} {6, 0} {inf, 0} 
  {-inf, 0} {4, 0} {4.5, 0.5} {5, 0} {inf, 0}
  */
  return 0;
}

int main()
{
  test_cast_pair<double>();
  test_landscape();
  return 0;
}
