#include <iostream>
#include <algorithm>
#include <functional>
#include <limits>
#include <cmath>
#include <vector>
#include <deque>
#include <list>
#include <boost/numeric/interval.hpp>
#include <boost/icl/discrete_interval.hpp>
#include <boost/icl/continuous_interval.hpp>

// float equality comparison
bool equals (const double& a, const double& b, double precision = 10e-09)
{
  return ( std::abs(a - b ) < precision);
}

double Infinity = std::numeric_limits<double>::infinity();
double minusInfinity = -1.0 * std::numeric_limits<double>::infinity();
using CritPoint = std::pair<double,double>;
using Interval = boost::icl::continuous_interval<double>;

// comparison class to implement the correct ordering of the intervals
struct intervalCompare
{
  inline bool operator() (const Interval& a, const Interval& b)
  {
    // its important to evaluate this first, because of the special nature of float equality
    // but this solution is not very elegant, because we have to evaluate the equality term 
    // for all cases, while in most of them it will be unneccessary
    // TODO: Ask for a better solution
    if ( (equals(lower(a), lower(b))) && (upper(a) > upper(b)) ) 
    {
      return true;
    }
    else if (lower(a) < lower(b))
    {
      return true;
    }
    else
    {
      return false;
    }
  }
};

std::vector<std::vector<CritPoint> > calcLandscape (std::deque<Interval> A)
{
  size_t k = 0;
  std::vector<std::vector<CritPoint> > L;

  std::sort(A.begin(), A.end(), intervalCompare());

  while ( !(A.empty()) )
  {
    // for (auto it : A ) { std::cout << it << " "; } std::cout << std::endl;
    L.push_back(std::vector<CritPoint>());
    assert(L.size() == k+1);
    // std::cout<< "check 1" << std::endl;
    Interval current = A.front();
    A.pop_front();
    double b = lower(current); // aufpassen ohne referenz funktioniert es evtl nicht richtig
    double d = upper(current);
    auto p = A.begin();
    
    //std::cout << *p << std::endl;
    //std::cout << "k is: " << k << " L[k]: " << std::endl;
    //std::cout << L.size() << std::endl;
    //std::cout <<"check 1.1"<< std::endl;
    L[k].push_back(CritPoint(minusInfinity,0.) );
    //std::cout<< "check 1.2" << std::endl;
    L[k].push_back(CritPoint(b,0.) );
    //std::cout<< "check 1.3" << std::endl;
    
    L[k].push_back(CritPoint( (b + d) / 2. , (d - b) / 2. ));
    
    while ( L[k].back() != CritPoint(Infinity,0.) ) // anders herum??
    {
      //std::cout<< "check 2" << std::endl;
      //for (auto it : A ) { std::cout << it << " "; } std::cout << std::endl;

      p = std::find_if(p, A.end(), [d] (auto interval)->bool {return interval.upper() > d;} ); //wsl hier segfault
      //std::cout << *p << std::endl;
      if ( p == A.end() )
      {
        L[k].push_back(CritPoint(d,0.));
        L[k].push_back(CritPoint(Infinity,0.));
      }
      else
      {
        double b_ = p->lower(); //std::cout<< "b_: " << b_<< " "; std::cout<< "b: " << b<< std::endl;
        double d_ = p->upper(); //std::cout<< "d_: " << d_<< " "; std::cout<< "d: " << d<< std::endl;
        auto h = p;
        //std::cout<< "h: " << *h << std::endl;
        p = A.erase(h);// evtl: erase p 
        //for (auto it : A ) { std::cout << it << " "; } std::cout << std::endl;
        //std::cout << *p << std::endl;
        if (equals (b_, d))
        {
          std::cout<< " b_ equals d" << std::endl;
          L[k].push_back(CritPoint(b_,0.) );
        }
        else if (b_ > d)
        {
          std::cout<< " b_ > d" << std::endl;
          L[k].push_back(CritPoint(d ,0.) );
          L[k].push_back(CritPoint(b_,0.) );
        }
        else
        {
          //std::cout<< " b_ < d" << std::endl; std::cout<< "b_: " << b_<< " "; std::cout<< "d: " << d<< std::endl;
          //std::cout << "füge ein: " << (b_+d) / 2. <<", "<< (d-b_) / 2. << std::endl;
          L[k].push_back(CritPoint( (b_+d) / 2. , (d-b_) / 2. ));
          //std::cout<< " p: " << *p<< std::endl;
          //std::cout<< " b_ < d" << std::endl; std::cout<< "b_: " << b_<< " "; std::cout<< "d: " << d<< std::endl;
          //for (auto it : A ) { std::cout << it << " "; } std::cout <<"check" << std::endl;
          //std::cout << "p: " <<*p << std::endl;
          A.insert(p, Interval(b_, d) );
          //for (auto it : A ) { std::cout << it << " "; } std::cout <<"check" << std::endl;
          //std::cout<< " b_ < d" << std::endl; std::cout<< "b_: " << b_<< " "; std::cout<< "d: " << d<< std::endl;
          //p++;
        }
        L[k].push_back(CritPoint( (b_+d_) / 2. , (d_-b_) / 2. ));
        b = b_; // kann nicht wirklich stimmen auf referenzen aufpassen
        d = d_; // s.o.
      }
    }
    ++k;
  }
  return L;
}

std::ostream& operator<< ( std::ostream& os, std::vector<std::vector<CritPoint> > L)
{
  for (auto Lk: L)
    {
      for (auto it : Lk)
      {
        os << "{"<<it.first<<", "<< it.second<< "}" << " ";
      }
      os << std::endl;
    }
    return os;
}
// TODO: in eigener headerdatei ablegen evtl mit eigenem namespace
template <typename T>
std::vector<T> operator* (const T& scalar, const std::vector<T>& vector)
{
  std::vector<T> result;
  for (auto x : vector)
  {
    result.push_back(scalar * x);
  }
  return result;
}

template <typename T>
std::vector<T> operator+ (const std::vector<T>& vector1, const std::vector<T>& vector2)
{
  std::vector<T> result;
  size_t N = std::min(vector1.size(), vector2.size());
  for (size_t i = 0; i < N; i++)
  {
    result.push_back(vector1[i] + vector2[i]);
  }
  return result;
}

template <typename T>
std::vector<T>& operator+= (std::vector<T>& vector1, const std::vector<T>& vector2)
{
  assert(vector1.size() == vector2.size());
  size_t N = std::min(vector1.size(), vector2.size());
  for (size_t i = 0; i < N; i++)
  {
    vector1[i] += vector2[i];
  }
  return vector1;
}

// TODO: implementieren
void evaluate(std::vector<std::vector<CritPoint> > L ) {}

// really ugly TODO: do this better
double evaluate(std::pair<std::vector<double>, std::vector<double> > XY, double x) 
{ 
  std::vector<double> X = XY.first;
  std::vector<double> Y = XY.second;
  
  auto xIter = std::lower_bound(X.begin(), X.end(), x);
  size_t index = xIter - X.begin();
  // linear interpolation
  double m = (Y[index] - Y[index - 1]) / (X[index] - X[index - 1]);
  return (Y[index] - (m * (X[index] - x) ));
}
// input schöner machen für beliebige anzahl von echten vektoren (oles letzte folie).
std::pair<std::vector<double>, std::vector<double> > calcLinComb ( std::vector<std::pair<std::vector<double>, std::vector<double> > > XY, std::vector<double> a)
{
  size_t N = XY.size();
  // merge the vectors evtl. with std::accumulate TODO: capsule this in a structure
  std::vector<double> X;
  for (auto XY_i : XY)
  {
    std::vector<double> tmp;
    std::vector<double> X_i = XY_i.first;
    std::merge(X.begin(), X.end(), X_i.begin(), X_i.end(), std::back_inserter(tmp) );
    X = tmp;
  }
  // erase duplicates
  std::unique(X.begin(), X.end(), [](const double& a, const double& b) { return equals(a,b); } );
  // calculate 
  std::vector<std::vector<double> > Y_;
  for (size_t j = 0; j < N; j++)
  {
    Y_.push_back(std::vector<double>() );
    for (auto x_i : X )
    {
      Y_[j].push_back( evaluate(XY[j], x_i) ); 
    }
  }
  
  
  std::vector<double> Y(X.size(),0); // = std::accumulate(Y_.begin(), Y_.end(), std::vector<double>(X.size(), 0), [a] (std::vector<double> Y1, std::vector<double> Y2) { return Y1 + a[j]  * Y2
  for (size_t j = 0; j < N; j++)
  {
    Y += (a[j] * Y_[j]);
  }
  
  return std::make_pair(X,Y);
}

std::pair<std::vector<double>, std::vector<double> > castLandscape_k(std::vector<CritPoint> input)
{
  std::vector<double> X;
  std::vector<double> Y;
  for (auto critPoint : input)
  {
    X.push_back(critPoint.first);
    Y.push_back(critPoint.second);
  }
  return std::make_pair(X,Y);
}

int main()
{
  std::deque<Interval> A = {Interval(1. , 5.), Interval(2. , 8.), Interval(3. , 4.), Interval(5. , 9.), Interval(6. , 7.), Interval(9.,10.) };
  auto a = calcLandscape(A);
  std::cout << a << std::endl;
  std::deque<Interval> B = {Interval(1. , 6.), Interval(2. , 3.), Interval(3. , 7.), Interval(4. , 5.) };
  auto b = calcLandscape(B);
  std::cout << b << std::endl;
  auto a_ = castLandscape_k(a[0]);
  auto b_ = castLandscape_k(b[0]);
  std::vector<double> alpha = {1,2};
  std::vector<std::pair<std::vector<double>, std::vector<double> >> inputHelper = {a_, b_};
  auto result = calcLinComb(inputHelper, alpha);
  
  
  
  return 0;
}
