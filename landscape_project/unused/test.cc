#include <iostream>
#include <set>
#include <string>
#include <vector>
#include <boost/icl/continuous_interval.hpp>

#include <boost/icl/interval_set.hpp>
#include <boost/icl/interval_map.hpp>
#include <boost/icl/separate_interval_set.hpp>
#include <boost/icl/split_interval_set.hpp>
#include <boost/icl/split_interval_map.hpp>

#include <cmath>



using namespace std;
using namespace boost;
using namespace boost::icl;


template <typename T = double, typename R = double>
class TriagonalFunctor
{
  double offset;
  double peak;
public:
  virtual R operator() (T x) const { return (x + offset) ? x+offset : -x-offset;} // nicht ganz richtig nochmal ausrechnen.
  virtual bool operator== (const TriagonalFunctor<T,R>& other) const 
  { 
    return (this->offset == other.getOffset()) ? true : false; // float comparison richtig machen, erstmal egal da eh alles unterschiedlich
  }
  TriagonalFunctor(double offset_, double peak_) : offset(offset_), peak(peak_) {}
  double getOffset() const {return this->offset;}
};

/* The most simple example of an interval_map is an overlap counter.
   If intervals are added that are associated with the value 1,
   all overlaps of added intervals are counted as a result in the
   associated values. 
*/
typedef split_interval_map<int, int> OverlapCounterT;

void print_overlaps(const OverlapCounterT& counter)
{
    for(OverlapCounterT::const_iterator it = counter.begin(); it != counter.end(); it++)
    {
        discrete_interval<int> itv  = (*it).first;
        int overlaps_count = (*it).second;
        if(overlaps_count == 1)
            cout << "in interval " << itv << " intervals do not overlap" << endl;
        else
            cout << "in interval " << itv << ": "<< overlaps_count << " intervals overlap" << endl;
    }
}

void overlap_counter()
{
    OverlapCounterT overlap_counter;
    discrete_interval<int> inter_val;

    inter_val = discrete_interval<int>::right_open(4,9);
    cout << "adding   " << inter_val << endl;
    overlap_counter += make_pair(inter_val, 1);
    print_overlaps(overlap_counter);

    inter_val = discrete_interval<int>::right_open(6,9);
    cout << "adding   " << inter_val << endl;
    overlap_counter += make_pair(inter_val, 1);
    print_overlaps(overlap_counter);


    inter_val = discrete_interval<int>::right_open(1,9);
    cout << "adding   " << inter_val << endl;
    overlap_counter += make_pair(inter_val, 1);
    print_overlaps(overlap_counter);


}
template<typename T>
ostream& operator<< (ostream& os, const std::vector<T>& rhs)
{
  os << '[';
  for ( auto i : rhs ) 
  { 
    os << i << ' ';
  }
  os << ']'<< endl;
  return os;
}

template <typename Type> struct inplace_append 
        : public identity_based_inplace_combine<Type>
    {
        typedef inplace_append<Type> type;

        void operator()(Type& object, const Type& operand)const
        { object.insert(object.end(), operand.begin(), operand.end() ); }

        static void version(Type&){}
    };


  template <typename Domain, typename Codomain, 
            typename Traits = boost::icl::partial_absorber,
            template<class>class Compare = std::less, 
            template<class>class Combine = inplace_append,
            template<class>class Section = boost::icl::inter_section>
            
  using interval_map_type = boost::icl::interval_map<Domain, Codomain, Traits, Compare, Combine, Section, continuous_interval<Domain> >;
    
void testInterval()
{
  using is = std::vector<TriagonalFunctor<double,double>>;
  
    
  continuous_interval<double> i1 
    = construct<continuous_interval<double> > (0,1);
  continuous_interval<double> i2
    = construct<continuous_interval<double> > (0.5,1.5);
  continuous_interval<double> i3
    = construct<continuous_interval<double> > (-0.5,0.5);
  
  boost::icl::split_interval_map<double, char >  intervals;
  interval_map_type<double, is>  intervals2;
 
  //intervals.add(make_pair(i1, 'a'));
  
  intervals.add(make_pair(i1, 'a'));
  intervals.add(make_pair(i2, 'b'));
  intervals.add(make_pair(i3, 'c'));

  is is1;
  is is2;
  is is3;
  
  TriagonalFunctor<double,double> a(1.,-0.2);
  TriagonalFunctor<double,double> b(2., 0.2);
  TriagonalFunctor<double,double> c(3., 1.3);
  
  is1.insert(is1.begin(), a);
  is2.insert(is2.begin(), b);
  is3.insert(is3.begin(), c);
  
  intervals2.add(make_pair(i1, is1));
  intervals2.add(make_pair(i2, is2));
  intervals2.add(make_pair(i3, is3));
  
  // cout << intervals << endl;
  // cout << intervals2 << endl;
  
  //cout << contains(intervals, 0)<< endl;
  //cout << contains(intervals, -0.4)<< endl;
  //cout << contains(intervals, 0.05)<< endl;
  //cout << (*(intervals.find(0.))).first<< (*(intervals.find(-0.8))).second <<endl;
  
  
  cout << intervals2.find(0.)->first <<endl;
  
  auto  p = intervals2.find(0.)->second;
  for ( auto i : p ) { cout << i.getOffset() << ' ';}
  cout << endl;
  
  cout << intervals2.find(0.5)->first <<endl;
  p = intervals2.find(0.5)->second;
  for ( auto i : p ) { cout << i.getOffset() << ' ';}
  cout << endl;
  
  cout << intervals2.find(1.4)->first <<endl;
  p = intervals2.find(1.4)->second;
  for ( auto i : p ) { cout << i.getOffset()<< ' ';}
  cout << endl;
  
  cout << intervals2.find(-0.5)->first <<endl;
  p = intervals2.find(-0.5)->second;
  for ( auto i : p ) { cout << i.getOffset()<< ' ';}
  cout << endl;
  
  cout << lower(i1) << endl;
  
}

void testStackoverflow()
{
}

int main()
{
  testInterval();
  testStackoverflow();
  return 0;
}
