template <typename T = double, typename R = double>
class TriagonalFunctor
{
private:
  T birth;
  T death;
  T peak;
  
public:
  virtual R operator() (T x) const 
  {
    if (x < birth || x > death)
    {
      return 0;
    }
    else if (x <= peak ) 
    {
      return x - b;
    }
    else 
    {
      return -x + d;
    }
  }
  virtual bool operator== (const TriagonalFunctor<T,R>& other) const 
  {
    return (this->offset == other.getOffset()) ? true : false; // float comparison richtig machen, erstmal egal da eh alles unterschiedlich
  }
  
  TriagonalFunctor(T birth_, T death_) : birth(birth_), death(death_) 
  {
    peak = (birth + death) / 2.;
  }
  TriagonalFunctor(T birth_, T death_, peak_) : birth(birth_), death(death_), peak(peak_) 
  {}
  
  T getBirth() const 
  {
    return this->birth;
  }
  
  T getDeath() const 
  {
    return this->death;
  }
  
  T getPeak() const 
  {
    return this->peak;
  }
};
