#include <vector>
#include <boost/icl/continuous_interval.hpp>
#include <boost/icl/split_interval_map.hpp>

template <typename Domain, typename Codomain, 
          typename Traits = boost::icl::partial_absorber,
          template<class>class Compare = std::less, 
          template<class>class Combine = inplace_append,
          template<class>class Section = boost::icl::inter_section>
using interval_map = boost::icl::interval_map<Domain, Codomain, Traits, Compare, Combine, Section, continuous_interval<Domain> >;
