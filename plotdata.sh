#!/bin/bash
data="$1"
output="$2"

gnuplot -persist <<-EOFMarker
	set terminal svg
	set $output
	plot for [k=0:*] \"$data\" index k using 1:2 with line notitle
EOFMarker