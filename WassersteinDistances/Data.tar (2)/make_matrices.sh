#!/usr/bin/env zsh

topological_distance --hausdorff --power=1 Sphere_r063/??.txt Torus_R025_r05/??.txt > Distance_matrix_sphere_torus_Hausdorff_1.txt

topological_distance --hausdorff --power=2 Sphere_r063/??.txt Torus_R025_r05/??.txt > Distance_matrix_sphere_torus_Hausdorff_2.txt

topological_distance --indicator --power=1 Sphere_r063/??.txt Torus_R025_r05/??.txt > Distance_matrix_sphere_torus_persistence_indicator_function_1.txt

topological_distance --indicator --power=2 Sphere_r063/??.txt Torus_R025_r05/??.txt > Distance_matrix_sphere_torus_persistence_indicator_function_2.txt

topological_distance --wasserstein --power=1 Sphere_r063/??.txt Torus_R025_r05/??.txt > Distance_matrix_sphere_torus_Wasserstein_1.txt

topological_distance --wasserstein --power=2 Sphere_r063/??.txt Torus_R025_r05/??.txt > Distance_matrix_sphere_torus_Wasserstein_2.txt
